#!/usr/bin/env sh

sleep 10 && ~/.local/bin/setdwmblocks &
nm-applet &
mpd &
remaps &		# run the remaps script, switching caps/esc and more; check it for more info
setbg ~/Pictures/Wallpapers &			# set the background with the `setbg` script
xrdb ${XDG_CONFIG_HOME:-$HOME/.config}/x11/xresources &	# Uncomment to use Xresources colors/settings on startup
picom --experimental-backends&
dunst &			# dunst for notifications
unclutter &		# Remove mouse when idle
fcitx &
emacs --daemon &
clash &
