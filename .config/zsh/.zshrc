# vim:filetype=zsh
# -*- mode: sh; -*-
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

vifmcd() {
    local dst="$(command ~/.config/vifm/scripts/vifmrun . --choose-dir - "$@")"
    if [ -z "$dst" ]; then
        echo 'Directory picking cancelled/failed'
        return 1
    fi
    cd "$dst"
}

bindkey -s '^o' 'vifmcd\n'
bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'
bindkey -s "^p" "^[[A"
bindkey -s "^n" "^[[B"

[ -s "/usr/bin/direnv" ] && eval "$(direnv hook zsh)"

exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

ncmpcppShow() {
    ncmpcpp <$TTY
    zle redisplay
}
zle -N ncmpcppShow
bindkey '^[\' ncmpcppShow

source /usr/share/doc/pkgfile/command-not-found.zsh

alias v="emacsclient -nw" \
    p="sudo pacman" \
    s="sudo systemctl" \
    us="systemctl --user" \
    uss="systemctl --user start" \
    use="systemctl --user enable --now" \
    usd="systemctl --user disable --now" \
    ust="systemctl --user stop --now" \
    ussr="systemctl --user restart" \
    spacemacs="chemacs spacemacs" \
    purcellemacs="chemacs purcell"

ee() {
    emacsclient -nc "$@"
    exit_zsh
}

yt() {
    cd ~/Videos
    youtube-dl "$@"
}

et() {
    export TERM='xterm-256color'
    emacsclient -nw "$@"
    export TERM='st-256color'
}

lg() {
    export LAZYGIT_NEW_DIR_FILE=~/.lazygit/newdir

    lazygit "$@"

    if [ -f $LAZYGIT_NEW_DIR_FILE ]; then
            cd "$(cat $LAZYGIT_NEW_DIR_FILE)"
            rm -f $LAZYGIT_NEW_DIR_FILE > /dev/null
    fi
}

zgit_info_update() {
    zgit_info=()

    local gitdir=$(git rev-parse --git-dir 2>/dev/null)
    if [ $? -ne 0 ] || [ -z "$gitdir" ]; then
        return
    fi

    # More code ...
}

# ~/gentoo/startprefix > /dev/null 2>&1
