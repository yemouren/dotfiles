" setting
set mouse-=a
set go-=a
set clipboard+=unnamedplus
set expandtab
set wrap
set autowrite
set confirm
set foldmethod=syntax
set foldlevel=3
set nocompatible
" set nu
" set relativenumber
let maplocalleader = "\\"
set cul
set smartindent
set noswapfile
set nobackup
set undofile
set undodir=~/.config/nvim/.undodir
set autowrite
set shortmess+=c
" maps
"
" 禁用方向键
map <Up> <nop>
map <Right> <nop>
map <Left> <nop>
map <Down> <nop>
imap <Up> <nop>
imap <Right> <nop>
imap <Left> <nop>
imap <Down> <nop>
nmap <leader>cp :w! \| !compiler "<c-r>%"<CR>
nmap <space>f :Files<CR>
map <localleader>ff migg=G'i
map S :%s//g<left><left>
map <localleader>at :e! ~/Work/TODO.md<CR>
imap jk <ESC>
nmap <space><space> /
imap <C-x><C-s> <ESC>:w<CR>a


nmap <leader>1 <Plug>lightline#bufferline#go(1)
nmap <leader>2 <Plug>lightline#bufferline#go(2)
nmap <leader>3 <Plug>lightline#bufferline#go(3)
nmap <leader>4 <Plug>lightline#bufferline#go(4)
nmap <leader>5 <Plug>lightline#bufferline#go(5)
nmap <leader>6 <Plug>lightline#bufferline#go(6)
nmap <leader>7 <Plug>lightline#bufferline#go(7)
nmap <leader>8 <Plug>lightline#bufferline#go(8)
nmap <leader>9 <Plug>lightline#bufferline#go(9)
nmap <leader>0 <Plug>lightline#bufferline#go(10)
nmap <leader>c1 <Plug>lightline#bufferline#delete(1)
nmap <leader>c2 <Plug>lightline#bufferline#delete(2)
nmap <leader>c3 <Plug>lightline#bufferline#delete(3)
nmap <leader>c4 <Plug>lightline#bufferline#delete(4)
nmap <leader>c5 <Plug>lightline#bufferline#delete(5)
nmap <leader>c6 <Plug>lightline#bufferline#delete(6)
nmap <leader>c7 <Plug>lightline#bufferline#delete(7)
nmap <leader>c8 <Plug>lightline#bufferline#delete(8)
nmap <leader>c9 <Plug>lightline#bufferline#delete(9)
nmap <leader>c0 <Plug>lightline#bufferline#delete(10)
"emacs 键位
inoremap <C-u> <ESC>daw

"autocmd
autocmd BufWritePost bm-files,bm-dirs !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufRead,BufNewFile xresources,xdefaults set filetype=xdefaults
autocmd BufWritePost Xresources,Xdefaults,xresources,xdefaults !xrdb %
" Recompile dwmblocks on config edit.
autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid -f dwmblocks }
autocmd BufWritePost ~/.local/src/dwm/config.h !cd ~/.local/src/dwm/; sudo make install && { kill -HUP $(pgrep -u $USER "\bdwm$")}
autocmd BufWritePost ~/.local/src/st/config.h !cd ~/.local/src/st/; sudo make install
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd VimLeave *.tex !texclear %
autocmd BufWritePre my_configs.vim exec ":call lightline#toggle()"
autocmd BufWritePost my_configs.vim exec ":call lightline#toggle()"
autocmd BufReadPre .spacemacs exec "set syntax=lisp"
autocmd BufWritePost *.go exec ":GoFmt"
autocmd BufWritePost *.go exec ":GoTest"
autocmd Filetype go nmap <leader>ct :GoTestFunc<CR>
autocmd Filetype go nmap <leader>cr :GoRun<CR>
autocmd Filetype go nmap <leader>cp :GoBuild<CR>
autocmd Filetype vimwiki exec ":set nospell"
aug QFClose
    au!
    au WinEnter * if winnr('$') == 1 && &buftype == "quickfix"|q|endif
aug END


"load plugins
call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
Plug 'jceb/vim-orgmode'
Plug 'godlygeek/tabular'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'lervag/vimtex'
Plug 'ap/vim-css-color'
Plug 'vimwiki/vimwiki'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'thinca/vim-quickrun'
Plug 'ryanoasis/vim-devicons'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'dracula/vim'
Plug 'yggdroot/indentline'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'kaicataldo/material.vim', { 'branch': 'main' }
Plug 'mhinz/vim-startify'
Plug 'neoclide/coc.nvim'
Plug 'kien/rainbow_parentheses.vim'
call plug#end()

let g:material_terminal_italics = 1
let g:material_theme_style = 'default-community'
color material
" let g:material_theme_style = 'default' | 'palenight' | 'ocean' | 'lighter' | 'darker' | 'default-community' | 'palenight-community' | 'ocean-community' | 'lighter-community' | 'darker-community'

nnoremap <silent> <leader> :<c-u>WhichKey  ','<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  '\'<CR>
nnoremap <silent> <space> :<c-u>WhichKey  '<space>'<CR>
let g:vim_markdown_math = 1
let g:tex_flavor = 'latex'
let g:vimtex_view_general_viewer = 'st -e zathura'
let g:vimtex_view_method = 'zathura'
let g:vimtex_quickfix_mode = 0
let g:vimtex_compiler_progname = 'nvr'
let g:vimwiki_list = [{'path': '~/Documents/vimwiki'}]
let g:tex_conceal = 'abdmg'
let g:livepreview_previewer = 'st -e zathura'
let g:livepreview_engine = 'xelatex'
let g:vimtex_compiler_latexmk_engines = {
            \ '_'                : '-xelatex',
            \ 'pdflatex'         : '-pdf',
            \ 'dvipdfex'         : '-pdfdvi',
            \ 'lualatex'         : '-lualatex',
            \ 'xelatex'          : '-xelatex',
            \ 'context (pdftex)' : '-pdf -pdflatex=texexec',
            \ 'context (luatex)' : '-pdf -pdflatex=context',
            \ 'context (xetex)'  : '-pdf -pdflatex=''texexec --xtx''',
            \}
let g:quickrun_config = {
            \   "_" : {
            \       "outputter" : "message",
            \   },
            \}

let g:webdevicons_enable_ctrlp = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:lightline#bufferline#shorten_path = 1

let g:lightline = {
            \ 'colorscheme': 'wombat',
            \'component_function': {
            \   'mode': 'LightlineMode',
            \   'syntastic': 'SyntasticStatuslineFlag',
            \'filename': 'LightlineFilename',
            \   'cocstatus': 'coc#status',
            \'gitbranch': 'FugitiveHead',
            \   'filetype': 'MyFiletype',
            \'blame': 'LightlineGitBlame',
            \   'fileformat': 'MyFileformat'
            \ },
            \ 'active': {
            \   'right': [ [ 'lineinfo' ],
            \              [ 'percent' ],
            \              ['blame'],
            \              [ 'fileformat', 'fileencoding']],
            \   'left': [ [ 'mode', 'paste' ],
            \             [  'fugitive','gitbranch','readonly',  'charvaluehex' ,'cocstatus' ,'modified'] ]
            \ },
            \ 'component': {
            \   'charvaluehex': '0x%B',
            \   'lineinfo': '%3l:%-2v%<',
            \ },
            \ 'separator': { 'left': ' ', 'right': ' ' },
            \ 'subseparator': { 'left': ' ', 'right': ' ' }
            \ }

let g:lightline.mode_map = {
            \ 'n' : 'NORMAL',
            \ 'i' : 'INSERT',
            \ 'R' : 'REPLACE',
            \ 'v' : 'VISUAL',
            \ 'V' : 'V-LINE',
            \ "\<C-v>": 'V-BLOCK',
            \ 'c' : 'COMMAND',
            \ 's' : 'SELECT',
            \ 'S' : 'S-LINE',
            \ "\<C-s>": 'S-BLOCK',
            \ 't': 'TERMINAL',
            \ }
let g:syntastic_mode_map = { 'mode': 'passive',
            \                      'active_filetypes': ['c', 'cpp'] }
let g:lightline.tabline          = {'left': [['buffers']],  'right': [['filetype', 'gitbranch']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers', 'tabs': 'lightline#tabs'}
let g:lightline.component_type   = {'buffers': 'tabsel'}
let g:lightline#bufferline#unnamed      = '[New buffer]'
let g:lightline#bufferline#unicode_symbols = 1
let g:lightline#bufferline#enable_devicons = 1
let g:snipMate = { 'snippet_version' : 1 }

function! LightlineMode()
    return expand('%:t') =~# '^__Tagbar__' ? 'Tagbar':
                \ expand('%:t') ==# 'ControlP' ? 'CtrlP' :
                \ &filetype ==# 'unite' ? 'Unite' :
                \ &filetype ==# 'vimfiler' ? 'VimFiler' :
                \ &filetype ==# 'vimshell' ? 'VimShell' :
                \ lightline#mode()
endfunction

function! LightlineFilename()
    return &filetype ==# 'vimfiler' ? vimfiler#get_status_string() :
                \ &filetype ==# 'unite' ? unite#get_status_string() :
                \ &filetype ==# 'vimshell' ? vimshell#get_status_string() :
                \ expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
endfunction

function! MyFiletype()
    return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
    return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

let g:unite_force_overwrite_statusline = 0
let g:vimfiler_force_overwrite_statusline = 0
let g:vimshell_force_overwrite_statusline = 0
nmap <leader>cr :QuickRun<CR>
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" set fcitx
" "##### auto fcitx  ###########
let g:input_toggle = 1
function! Fcitx2en()
    let s:input_status = system("fcitx-remote")
    if s:input_status == 2
        let g:input_toggle = 1
        let l:a = system("fcitx-remote -c")
    endif
endfunction

function! Fcitx2zh()
    let s:input_status = system("fcitx-remote")
    if s:input_status != 2 && g:input_toggle == 1
        let l:a = system("fcitx-remote -o")
        let g:input_toggle = 0
    endif
endfunction

hi Folded guibg=black guifg=grey40 ctermfg=grey ctermbg=darkgrey

set ttimeoutlen=150
"退出插入模式
autocmd InsertLeave * call Fcitx2en()
"进入插入模式
autocmd InsertEnter * call Fcitx2zh()
"##### auto fcitx end ######
let g:vimspector_enable_mappings = 'HUMAN'

function! LightlineTabs() abort
    let [x, y, z] = [[], [], []]
    let nr = tabpagenr()
    let cnt = tabpagenr('$')
    for i in range(1, cnt)
        call add(i < nr ? x : i == nr ? y : z,
                    \ '%' . i . '%%{lightline#onetab(' . i . ',' . (i == nr) . ')}'
                    \ . (i == cnt ? '%T' : ''))
    endfor
    if len(x) > 3
        let x = x[len(x)-3:]
        let x[0] = '<' . x[0]
    endif
    if len(z) > 3
        let z = z[:2]
        let z[len(z)-1] = z[len(z)-1] . '>'
    endif
    return [x, y, z]
endfunction
function! LightlineGitBlame() abort
    let blame = get(b:, 'coc_git_blame', '')
    " return blame
    return winwidth(0) > 120 ? blame : ''
endfunction
" packadd! vimspector
let g:go_list_type = "quickfix"
let g:go_test_timeout = '10s'
let g:go_fmt_command = "goimports"
let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
let g:go_metalinter_autosave = 1
let g:go_metalinter_autosave_enabled = ['vet', 'golint']
let g:go_metalinter_deadline = "5s"
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
let g:cpp_experimental_simple_template_highlight = 1
let g:cpp_concepts_highlight = 1
let c_no_curly_error=1


let g:startify_bookmarks = [
            \{'mc': '~/.vim_runtime/my_configs.vim'},
            \{'zc': '~/.config/zsh/config'},
            \{'stc': '~/.local/src/st/config.h'}
            \]

let g:startify_commands = [
            \ {'ch': 'h coc'},
            \ ]
let g:startify_change_to_dir = 1

" ino <C-q> <C-r>=snipMate#TriggerSnippet()<cr>
" snor <C-q> <esc>i<right><C-r>=snipMate#TriggerSnippet()<cr>

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
else
    inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
            \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
nnoremap <silent> <space>cw :<C-u>CocList words<CR>
nnoremap <silent> <space>cl :<C-u>CocList<CR>
nnoremap <silent> <space>co :<C-u>CocList outline<CR>
nnoremap <silent> <space>cb :<C-u>CocList buffers<CR>
nnoremap <silent> <space>cf :<C-u>CocList files<CR>
nnoremap <silent> <space>cc :<C-u>CocList commands<CR>
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<s-tab>'

" Use <C-j> for both expand and jump (make expand higher priority.)
imap <C-j> <Plug>(coc-snippets-expand-jump)

" Use <leader>x for convert visual selected code to snippet
xmap <leader>x  <Plug>(coc-convert-snippet)
inoremap <silent><expr> <TAB>
            \ pumvisible() ? coc#_select_confirm() :
            \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
