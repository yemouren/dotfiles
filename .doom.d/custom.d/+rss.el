;;; custom.d/+rss.el -*- lexical-binding: t; -*-

;; elfeed configure
(setq elfeed-feeds
      '(
        "https://www.gcores.com/rss"
        "http://www.dapenti.com/blog/rss2.asp"
        "https://juejin.im/rss"
        "https://www.shuge.org/feed/"
        "http://www.dushumashang.com/feed"
        "https://zhainanbsa.net/feed"
        "https://research.sinica.edu.tw/feed/"
        "https://rsshub.app/zhihu/daily"
        "https://iseex.github.io/feed.xml"
        "https://www.sciencenews.org/feed"
        "https://science.sciencemag.org/rss/current.xml"
        "http://www.zreading.cn/feed"
        "http://cn.engadget.com/rss.xml"
        "http://feeds.feedburner.com/ruanyifeng"
        ("http://www.zhihu.com/rss" zhihu)
        "http://www.acgpiping.net/feed/"mhu
        "https://feedx.net/rss/wikiindex.xml"
        "http://lukesmith.xyz/rss.xml"
        "https://notrelated.libsyn.com/rss"
        ("https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA" LukeSmith)
        "https://www.archlinux.org/feeds/news"
        ("https://www.youtube.com/feeds/videos.xml?channel_id=UCLgGLSFMZQB8c0WGcwE49Gw" Gamker_攻壳)
        ("https://www.marxists.org/chinese/feed.xml" 马克思中文文库))
      )

(set 'rmh-elfeed-org-files "~/org/elfeed.org")
