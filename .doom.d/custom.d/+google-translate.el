;;; custom.d/+google-translate.el -*- lexical-binding: t; -*-

(defun google-translate--search-tkk ()
  "Search TKK."
  (list 430675 2721866130))

(setq google-translate-base-url
      "https://translate.google.com/translate_a/single"
      google-translate--tkk-url
      "https://translate.google.com/"
      google-translate-default-source-language "en"
      google-translate-default-target-language "zh_CN")

(use-package! google-translate
  :bind
  ("C-c t". google-translate-at-point))

(provide '+google-translate)
