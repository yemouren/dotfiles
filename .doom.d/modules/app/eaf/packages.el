;; -*- no-byte-compile: t; -*-
;;; app/eaf/packages.el

;; eaf itself
(package! eaf
  :recipe (:host github
           :repo "manateelazycat/emacs-application-framework"))
;; eaf依赖
(package! ctable)
(package! deferred)
(package! epc)
(package! s)
