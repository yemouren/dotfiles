;; -*- no-byte-compile: t; -*-
;;; tools/el2org/packages.el

(package! el2org)
;; ox-gfm is needed by `el2org-generate-readme', if ox-gfm can not be found, ox-md will be used as fallback.
(package! ox-gfm)
