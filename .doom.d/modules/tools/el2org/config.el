;;; tools/el2org/config.el -*- lexical-binding: t; -*-

(use-package! el2org
  :after emacs-lisp)
