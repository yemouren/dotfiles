;; -*- no-byte-compile: t; -*-
;;; tools/skewer-mode/packages.el

(package! simple-httpd)
(package! js2-mode)
(package! skewer-mode)
