;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Ye Peng"
      user-mail-address "yepeng@yemouren.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "FiraCode Nerd Font" :size 17 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "monospace" :size 17)
      doom-big-font (font-spec :family "FiraCode Nerd Font" :size 30 :weight 'semi-light))

(setq doom-unicode-font (font-spec :family "FiraCode Nerd Font"))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)
(setq org-hide-emphasis-markers t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/"
      org-journal-file-format "%Y-%m-%d.org"
      org-journal-date-prefix "#+TITLE: "
      org-roam-directory "~/org/roam")

(setq org-log-done 'note)

(setq deft-directory "~/org/notes"
      deft-extensions '("org" "txt" "md")
      deft-recursive t)

(setq which-key-idle-delay 0.2)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

(setq pdf-view-midnight-colors '("#839496" . "#002b36" ))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(use-package! site-gentoo)

(add-hook 'emacs-startup-hook
          #'global-pretty-mode)

(after! company
  (setq! company-minimum-prefix-length 2
         company-idle-delay 0.2))


(add-hook! (cc-mode c-mode c++-mode)
  (rainbow-mode))
;; (add-hook 'after-init-hook #'toggle-frame-fullscreen)

(setq! fancy-splash-image "~/.doom.d/splash.png"
       confirm-kill-emacs nil
       flyspell-default-dictionary "en_US")

(use-package! org-super-agenda
  :after org-agenda
  :config
  (setq org-super-agenda-groups
        '(;; Each group has an implicit boolean OR operator between its selectors.
          (:name "Today"               ; Optionally specify section name
           :time-grid t                ; Items that appear on the time grid
           :todo "TODAY")              ; Items that have this TODO keyword
          (:name "Important"
           ;; Single arguments given alone
           :tag "bills"
           :priority "A")
          ;; Set order of multiple groups at once
          (:order-multi (2 (:name "Shopping in town"
                            ;; Boolean AND group matches items that match all subgroups
                            :and (:tag "shopping" :tag "@town"))
                           (:name "Food-related"
                            ;; Multiple args given in list with implicit OR
                            :tag ("food" "dinner"))
                           (:name "Personal"
                            :habit t
                            :tag "personal")
                           (:name "Space-related (non-moon-or-planet-related)"
                            ;; Regexps match case-insensitively on the entire entry
                            :and (:regexp ("space" "NASA")
                                  ;; Boolean NOT also has implicit OR between selectors
                                  :not (:regexp "moon" :tag "planet")))))
          ;; Groups supply their own section names when none are given
          (:todo "WAITING" :order 8)   ; Set order of this section
          (:todo ("SOMEDAY" "TO-READ" "CHECK" "TO-WATCH" "WATCHING" "NOTE")
           ;; Show this group at the end of the agenda (since it has the
           ;; highest number). If you specified this group last, items
           ;; with these todo keywords that e.g. have priority A would be
           ;; displayed in that group instead, because items are grouped
           ;; out in the order the groups are listed.
           :order 9)
          (:priority<= "B"
           ;; Show this section after "Today" and "Important", because
           ;; their order is unspecified, defaulting to 0. Sections
           ;; are displayed lowest-number-first.
           :order 1))))

(use-package! valign
  :config
  (add-hook 'org-mode-hook #'valign-mode)
  )

(use-package! pangu-spacing
  :hook (after-init . global-pangu-spacing-mode))

(use-package! all-the-icons-ivy-rich
  :config
  (add-hook 'after-init-hook #'all-the-icons-ivy-rich-mode))

(use-package! evil
  :config
  (defalias 'evil-insert-state 'evil-emacs-state)
  (define-key evil-emacs-state-map (kbd "<escape>") 'evil-normal-state)
  (setq evil-emacs-state-cursor 'bar))

(load! "~/.doom.d/custom.d/+calendar")
(load! "~/.doom.d/custom.d/+rss")
(load! "~/.doom.d/custom.d/+google-translate")

;; opacity
(doom/set-frame-opacity 75)

(use-package geiser
  :config
  (setq geiser-active-implementations '(racket)))

(setq +format-on-save-enabled-modes
      '(not sql-mode                    ; sqlformat is currently broken
            tex-mode                    ; latexindent is broken
            latex-mode))

(use-package! super-save
  :config
  (super-save-mode +1)
  (setq auto-save-default nil)
  (setq super-save-auto-save-when-idle t)
  (add-to-list 'super-save-triggers 'ace-window)
  ;; save on find-file
  (add-to-list 'super-save-hook-triggers 'find-file-hook))
