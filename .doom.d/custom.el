(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(send-mail-function 'mailclient-send-it)
 '(smtpmail-smtp-server "mail.yemouren.com")
 '(smtpmail-smtp-service 25))
(custom-set-faces!
  '(hl-fill-column-face :background "red"
                        :foreground "blue"
                        :inverse-video t))
